<?php

$params = require(__DIR__ . '/params.php');
defined('SAMARTH_PROJECT_ENV') or define('SAMARTH_PROJECT_ENV','dev');
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'queue'],
    'layout' => 'samarth2/main',
    'language'=>'en',
    'timeZone'=>'Asia/Kolkata',
    'components' => [
        'request' => [
        	//'trustedHosts' => [ '172.0.0.0/16'],  Your VPC Address for chrome update

            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'mYv7xrMawFPAPW8nMwLBAeXkTekp0wm4ghasidasGuru$%$^%',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
    //             'secureHeaders' => [
    //     'X-Forwarded-For',
    //     'X-Forwarded-Host',
    //     'X-Forwarded-Proto',
    //     'X-Proxy-User-Ip',
    //    // 'Front-End-Https',
    // ],
    // 'ipHeaders' => [
    //     'X-Proxy-User-Ip',
    // ],
    // 'secureProtocolHeaders' => [
    //     'Front-End-Https' => ['on']
    // ],
        ],
       'formatter' => [

            'class' => 'uims\core\modules\core\helper\Formatter',

            'nullDisplay' => '',

            'defaultTimeZone' => 'Asia/Kolkata',

        ],
        'authManager' => [

            'class' => 'uims\user\modules\jiuser\components\rbac\DbManager',

        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => '\uims\user\modules\jiuser\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/login'],
            'as authLog' => [
                'class' => 'yii2tech\authlog\AuthLogWebUserBehavior'
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'db' => require(__DIR__ . '/db.php'),
            'sessionTable' => 'yii_session',
            'writeCallback' => function($session){
                return [
                    'user_id' => Yii::$app->user->id
                ];
            },
            /*'cookieParams' => [
                'path' => '/',
                'domain' => "demo.samarth.ac.in",
                'httpOnly' => true,
            ],*/
        ],
        'mailer' => [
            'class' => 'app\components\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
//            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace', 'info'],
                    'maxLogFiles'=>500,
                ],
            ],
        ],
	 'db' => require(__DIR__ . '/db.php'),
	'db_uims' => require(__DIR__ . '/db_uims.php'),
        'db_student' => require(__DIR__ . '/db_student.php'),
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
           // 
                        ],
        'button' => [
            'class' => 'app\components\Button',
        ],
        'dateformatter' => [
            'class' => 'app\components\DateFormatter',
        ],
        'statusHelper' => [
            'class' => 'app\components\StatusHelper',
        ],
        'sms'=>[
            'class' => 'app\components\SmsComponent'
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
	'@uims' => '@vendor/uims',
    ],
'modules' => [

        'admission' => [
            'class' => 'app\modules\admission\Module',
        ],
	    'jiuser' => [
            'class' => 'uims\user\modules\jiuser\JiUser',
        ],
//        'login' => [
//            'class' => 'uims\login\modules\login\Module',
//        ],
        'department' => [
            'class' => 'app\modules\department\Modules',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'dynagrid'=>[
            'class'=>'\kartik\dynagrid\Module',
            // other settings (refer documentation)
        ],
        'dynagridCustom' =>  [
            'class' => '\kartik\dynagrid\Module',
            // your other dynagrid module settings
        ],
        'master' => [
            'class' => 'uims\core\modules\core\Module',
        ],
        'dashboard' => [
            'class' => 'app\modules\dashboard\Module',
        ],
        'student' => [
            'class' => 'uims\student\modules\student\Module',
        ],
        'notification' => [
            'class' => 'app\modules\notification\Module',
        ],
        'file_import' => [
            'class' => 'app\modules\file_import\Module',
        ],

        'legal' => [
            'class' => 'uims\legal\modules\legal\Module',
        ],
        'personal' => [
            'class' => 'app\modules\personal\Module',
        ],
        'rti' => [
            'class' => 'uims\rti\modules\rti\Module',
        ],
        'leave' => [
            'class' => 'uims\leave\modules\leave\Module',
        ],

        'estate' => [
            'class' => 'uims\estate\modules\estate\Module',
        ],
        'fee' => [
            'class' => 'uims\fee\modules\fee\Module',
        ],
        'cas' => [
            'class' => 'app\modules\cas\Module',
        ],
        'employee' => [
            'class' => 'uims\employee\modules\employee\Module',
        ],
        'rpms' => [
            'class' => 'uims\rpms\modules\rpms\Module',
        ],
        'settings' => [
            'class' => 'app\modules\settings\Module',
        ],
        'vendor_purchase' => [
            'class' => 'uims\vendor\modules\vendor_purchase\Module',
        ],
        'fmts' => [
            'class' => 'uims\fmts\modules\fmts\Module',
        ],
        'mail_server' => [
            'class' => 'app\modules\mail_server\Module',
        ],
        'sms_server' => [
            'class' => 'app\modules\sms_server\Module',
        ],
        'ims' => [
            'class' => 'uims\ims\modules\ims\Module',
        ],
        'payroll' => [
            'class' => 'uims\payroll\modules\payroll\Module',
        ],
        'admission' => [
            'class' => 'app\modules\admission\Module',
        ],
        'program' => [
            'class' => 'uims\programme\modules\program\Module',
        ],
        'academic' => [
            'class' => 'uims\academic\modules\academic\Module',
        ],
        'recruitment' => [
            'class' => 'uims\recruitment\modules\recruitment\Module',
        ],
        'service' => [
            'class' => 'uims\service\modules\service\Module',
        ],
        'training' => [
            'class' => 'uims\placement_admin\modules\training\Module',
        ],
        'transport' => [
            'class' => 'uims\fleet\modules\fleet\Module',
        ],
        'knowledge' => [
            'class' => 'uims\knowledge\modules\knowledge\Module',
        ],
        'health' => [
            'class' => 'uims\health\modules\health\Module',
        ],
        'hostel' => [
            'class' => 'uims\hostel_admin\modules\hostel\Module',
        ],
	'cfs' => [
	    'class' => 'uims\cfs\modules\cfs\Module',
	],
	'security'=>[
		'class' => 'uims\security\modules\security\Module',
	],
        'sports' => [
            'class' => 'uims\sports\modules\sports\Module',
        ],
        'grievance' => [
            'class' => 'uims\grievance\modules\grievance\Module',
        ],
        'ocm' => [
            'class' => 'uims\ocm\modules\ocm\Module',
        ],
        'v1' => [
            'class' => 'uims\api\modules\v1\Module',
        ],
        'feedback' => [
            'class' => 'uims\feedback\modules\feedback\Module',
        ],
        'document' => [
            'class' => 'uims\document\modules\document\Module',
        ],
        'affiliation' => [
            'class' => 'uims\affiliation\modules\affiliation\Module',
        ],
        'essential' => [
            'class' => 'uims\essential\modules\essential\Module',
        ],
        'endowment' => [
            'class' => 'uims\endowment\modules\endowment\Module',
        ],
        'endowment_settings' => [
            'class' => 'uims\endowment\modules\settings\Module',
        ],
        'endowment_payment' => [
            'class' => 'uims\endowment\modules\payment\Module',
        ],
        'ccs' => [
            'class' => 'uims\ccs\modules\ccs\Module',
        ],
        'alumni_administration' => [
            'class' => 'uims\alumni\modules\alumni_administration\Module',
        ],
        'alumni' => [
            'class' => 'uims\alumni\modules\alumni\Module',
        ],
        'tot' => [
            'class' => 'uims\tot\modules\tot\Module',
        ],
        'ehousing' =>  [
            'class' => 'uims\ehousing\modules\ehousing\Module',
        ],
        'itsd' =>  [
            'class' => 'uims\itsd\modules\itsd\Module',
        ],
        'cas' =>  [
            'class' => 'uims\cas\modules\cas\Module',
        ],
	 'evaluation' => [
            'class' => 'uims\evaluation\modules\evaluation\Module',
        ],
         'slcm' => [
            'class' => 'uims\student\modules\slcm\Module',
        ],
        'cdu' => [
            'class' => 'uims\cdu\modules\cdu\Module',
        ],
    ],
    'params' => $params,
];

\Yii::$container->set('yii\widgets\LinkPager', [
    'linkContainerOptions' => ['class' => 'page-item'],
    'linkOptions' => ['class' => 'page-link'],
    'nextPageLabel' => false,
    'prevPageLabel' => false,
]);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1','10.107.107.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1','10.107.107.143'],
    ];
}

return $config;
