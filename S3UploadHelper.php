<?php

namespace app\components\helper;

use app\models\ApplicationControl;
use app\models\SecurityHelper;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class S3UploadHelper
{
    public static function uploadFile($file, $document_id, $name, $module_name)
    {
        if (ApplicationControl::getVariable('upload_method') === 'S3') {
            return self::uploadFileS3($file, $document_id, $name, $module_name);
        } else {
            return \app\models\UploadHelper::uploadFileServer($file, $document_id, $name, $module_name);

        }


    }

    public static function uploadFileS3($file, $document_id, $name, $module_name)
    {
        try {
            $s3Client = new S3Client([
                'region' => ApplicationControl::getVariable('aws_s3_region'),
                'version' => 'latest',
                'credentials' => [
                    'key' => ApplicationControl::getVariable('aws_s3_key'),
                    'secret' => ApplicationControl::getVariable('aws_s3_secret_key'),
                ],
                'http' => [
                    'verify' => false
                ]
            ]);

            $result = $s3Client->putObject([
                'Bucket' => ApplicationControl::getVariable('aws_s3_upload_bucket'),
                'Key' => ApplicationControl::getVariable('upload_folder_name') . "/$module_name/" . SecurityHelper::hashData($document_id) . '/' . $name,
                'SourceFile' => $file->tempName,
                'ACL' => 'public-read',
                'ContentType' => $file->type
            ]);

            return [
                'status' => true,
                'url' => $result['ObjectURL']
            ];
        } catch (S3Exception $e) {
            var_dump($e->getMessage());
            die;
            return [
                'status' => false,
                'url' => ''
            ];
        }
    }

    public static function uploadDirectFileS3($file, $document_id, $name, $module_name)
    {
        try {
            $s3Client = new S3Client([
                'region' => ApplicationControl::getVariable('aws_s3_region'),
                'version' => 'latest',
                'credentials' => [
                    'key' => ApplicationControl::getVariable('aws_s3_key'),
                    'secret' => ApplicationControl::getVariable('aws_s3_secret_key'),
                ],
                'http' => [
                    'verify' => false
                ]
            ]);

            $result = $s3Client->putObject([
                'Bucket' => ApplicationControl::getVariable('aws_s3_upload_bucket'),
                'Key' => ApplicationControl::getVariable('upload_folder_name') . "/$module_name/" . SecurityHelper::hashData($document_id) . '/' . $name,
                'SourceFile' => $file,
                'ACL' => 'public-read',
                'ContentType' => '*/*'
            ]);

            return [
                'status' => true,
                'url' => $result['ObjectURL']
            ];
        } catch (S3Exception $e) {
            var_dump($e->getMessage());
            die;
            return [
                'status' => false,
                'url' => ''
            ];
        }
    }

    public static function uploadFileBase64($file, $formNo, $name)
    {
        try {
            $s3Client = new S3Client([
                'region' => ApplicationControl::getVariable('aws_s3_region'),
                'version' => 'latest',
                'credentials' => [
                    'key' => ApplicationControl::getVariable('aws_s3_key'),
                    'secret' => ApplicationControl::getVariable('aws_s3_secret_key'),
                ],
                'http' => [
                    'verify' => false
                ]
            ]);

            $result = $s3Client->putObject([
                'Bucket' => ApplicationControl::getVariable('aws_s3_upload_bucket'),
                'Key' => \Yii::$app->security->hashData($formNo, ApplicationControl::getVariable('admission_hash_key')) . '/' . $name,
                'Body' => $file,
                'ACL' => 'public-read',
                'ContentType' => 'image/jpeg'
            ]);

            return [
                'status' => true,
                'url' => $result['ObjectURL']
            ];
        } catch (S3Exception $e) {
            return [
                'status' => false,
                'url' => ''
            ];
        }
    }

    public static function uploadFileByFileURL($path, $file, $document_id, $name, $module_name)
    {
        if (ApplicationControl::getVariable('upload_method') === 'SERVER') {
            return \app\models\UploadHelper::uploadFileServerPdf($path,$file, $document_id, $module_name);

        } else {
            return self::uploadFileByFileURLS3($path . $file, $document_id, $name, $module_name);
        }

    }

    public static function uploadFileByFileURLS3($file, $document_id, $name, $module_name)
    {
        try {
            $s3Client = new S3Client([
                'region' => ApplicationControl::getVariable('aws_s3_region'),
                'version' => 'latest',
                'credentials' => [
                    'key' => ApplicationControl::getVariable('aws_s3_key'),
                    'secret' => ApplicationControl::getVariable('aws_s3_secret_key'),
                ],
                'http' => [
                    'verify' => false
                    
                    
                ]
            ]);

            $result = $s3Client->putObject([
                'Bucket' => ApplicationControl::getVariable('aws_s3_upload_bucket'),
                'Key' => ApplicationControl::getVariable('upload_folder_name') . "/$module_name/" . SecurityHelper::hashData($document_id) . '/' . $name,
                'SourceFile' => $file,
                'ACL' => 'public-read',
                'ContentType' => '*/*'
            ]);

            return [
                'status' => true,
                'url' => $result['ObjectURL']
            ];
        } catch (S3Exception $e) {
            return [
                'status' => false,
                'url' => ''
            ];
        }
    }
}