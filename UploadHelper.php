<?php

namespace app\components\helper;

use app\models\ApplicationControl;
use app\models\SecurityHelper;

class UploadHelper
{
    /**
     * PreviewUrl
     * @param String $userFolder
     * @return String full path http://path/id
     */
    public static function getPreviewUrl($id)
    {
        return ApplicationControl::getVariable('file_upload_preview_url') . "user/" . SecurityHelper::hashData($id);
    }

    public static function getPreviewUrlLocal($id)
    {
        return ApplicationControl::getVariable('fileLocalServerPath') . "user/" . SecurityHelper::hashData($id);
    }

    /**
     * upload folder path
     * @param String $userFolder
     * @return String full upload path
     */
    public static function getUploadFolderPath($userFolder)
    {
        return ApplicationControl::getVariable('file_upload_folder') . "user/" . $userFolder;
    }

    /**
     * Set upload path.
     * @param String $action full action path eg: /module/controller/actionId
     * @return String full upload path
     */
    public static function setUploadAction($action)
    {
        return ApplicationControl::getVariable('file_upload_server_base_path') . $action;
    }

    /**
     * Set delete path.
     * @param String $action full action path eg: /module/controller/actionId
     * @return String full delete path
     */
    public static function setDeleteAction($action)
    {
        return ApplicationControl::getVariable('file_upload_server_base_path') . $action;
    }

    /**
     * Update record
     * @param todo
     * @return integer
     */
    public static function updatePersonal($tableName, $column, $value, $id)
    {
        $connection = \Yii::$app->db;
        if (!empty($id) && !empty($column) && !empty($tableName)) {
            try {
                return $connection->createCommand()->update($tableName, [
                    $column => $value,
                ], 'id = :id', [':id' => $id])->execute();

            } catch (\Exception $e) {
                \Yii::error('Value cannot be saved in database.');
                return false;
            }
        } else {
            \Yii::error('Invalid id.' . $id);
            return false;
        }

    }

    /**
     * Update record
     * @param todo
     * @return integer
     */
    public static function updateAcademic($tableName, $column, $value, $id)
    {
        $connection = \Yii::$app->db;
        if (!empty($id) && !empty($column) && !empty($tableName)) {
            try {
                return $connection->createCommand()->update($tableName, [
                    $column => $value,
                ], 'formNo = :id', [':id' => $id])->execute();

            } catch (\Exception $e) {
                \Yii::error('Value cannot be saved in database.');
                return false;
            }
        } else {
            \Yii::error('Invalid id.' . $id);
            return false;
        }

    }

    /**
     * Update record
     * @param todo
     * @return integer
     */
    public static function updateEca($tableName, $column, $value, $id)
    {
        $connection = \Yii::$app->db;
        if (!empty($id) && !empty($column) && !empty($tableName)) {
            try {
                return $connection->createCommand()->update($tableName, [
                    $column => $value,
                ], 'id = :id', [':id' => $id])->execute();

            } catch (\Exception $e) {
                \Yii::error('Value cannot be saved in database.');
                return false;
            }
        } else {
            \Yii::error('Invalid id.' . $id);
            return false;
        }

    }

    /**
     * Update record
     * @param todo
     * @return integer
     */
    public static function updateSports($tableName, $column, $value, $id)
    {
        $connection = \Yii::$app->db;
        if (!empty($id) && !empty($column) && !empty($tableName)) {
            try {
                return $connection->createCommand()->update($tableName, [
                    $column => $value,
                ], 'id = :id', [':id' => $id])->execute();

            } catch (\Exception $e) {
                \Yii::error('Value cannot be saved in database.');
                return false;
            }
        } else {
            \Yii::error('Invalid id.' . $id);
            return false;
        }

    }

    public static function uploadToFileServer($url, $filePath, $fileType, $fileName, $fieldName, $key, $uploadPath)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $fields = [
            $fieldName => new \CurlFile($filePath, $fileType, $fileName),
            'key' => $key,
            'path' => $uploadPath,
            'fieldName' => $fieldName,
        ];

        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }
        curl_close($ch);
        var_dump($error_msg);
        die;
        return true;
    }

    public static function uploadFileServerPdf($sourcepath, $file, $document_id, $module_name)
    {
        $append_path = $module_name;
        $path = ApplicationControl::getVariable('file_upload_folder') . $append_path;

        if (!empty($path)) {
            $dir = is_dir($path);
            if ($dir == false) {
                $split_path = explode("/", $path);
                $str = '';
                $count = 0;
                $nstr = "";
                foreach ($split_path as $key => $path) {

                    if ($count++ == 0) {
                        $nstr .= $str . $path;
                    } else {
                        $nstr .= $str . '/' . $path;
                    }
                    $str = exec('mkdir ' . $nstr);
                }

                exec("chmod -R 777 $str");

            }
            if (!empty($file)) {
                try {
                    $a = copy($sourcepath . $file, $path . "/" . $file);

                    if ($a) {
                        return ['status' => true,
                            'url' => ApplicationControl::getVariable('file_upload_preview_url') . $append_path . '/' . $file
                        ];
                    } else {
                        return [
                            'status' => false,
                            'url' => ''
                        ];
                    }
                } catch (\Exception $e) {
                    return [
                        'status' => false,
                        'url' => ''
                    ];
                }

            } else {
                return [
                    'status' => false,
                    'url' => ''
                ];
            }

        } else {
            return [
                'status' => false,
                'url' => ''
            ];
        }
    }
}